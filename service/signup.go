package service

import (
	"bitbucket.org/lumos-hackaton/protobuf/generated"
	"bitbucket.org/lumos-hackaton/protobuf/generated/auth"
	"bitbucket.org/lumos-hackaton/shared/dbase"
	"bitbucket.org/lumos-hackaton/shared/dbase/entity/user"
	"bitbucket.org/lumos-hackaton/shared/json"
	"context"
	"errors"
	"github.com/micro/go-micro/client"
	"golang.org/x/crypto/bcrypt"
)

func (s *authService) Register(ctx context.Context, in *auth.RegisterRequest, opts ...client.CallOption) (*generated.String, error) {
	if r, err := s.IsEmailFreeCustom(in.Email); err == nil && !r {
		return nil, errors.New("Email is already registered")
	} else if err != nil {
		return nil, err
	}

	if r, err := s.IsUsernameFreeCustom(in.Username); err == nil && !r {
		return nil, errors.New("Username is already registered")
	} else if err != nil {
		return nil, err
	}

	u := user.NewUser()

	tempPass, err := bcrypt.GenerateFromPassword([]byte(in.Password), 8)
	if err != nil {
		return nil, err
	}

	u.Password = string(tempPass)

	u.Token, err = s.GenerateToken(in.Email, in.Password)
	if err != nil {
		return nil, err
	}

	u.Username = in.Username
	u.Email = in.Email

	err = dbase.Users.User(s.db).Insert(u)
	if err != nil {
		return nil, err
	}

	response, err := json.Marshal(&u)
	if err != nil {
		return nil, err
	}

	return &generated.String{Str: string(response)}, nil
}
