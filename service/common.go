package service

import (
	"bitbucket.org/lumos-hackaton/shared/dbase"
	"bitbucket.org/lumos-hackaton/protobuf/generated/auth"
)

type authService struct {
	db dbase.Database
}

func NewAuthService(db dbase.Database) auth.AuthService {
	return &authService{
		db: db,
	}
}