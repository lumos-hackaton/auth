package service

import (
	"bitbucket.org/lumos-hackaton/protobuf/generated"
	"context"
	"github.com/micro/go-micro/client"
)

func (s *authService) ResetPassword(ctx context.Context, in *generated.Email, opts ...client.CallOption) (*generated.EmptyMessage, error) {
	panic("implement me")
}

func (s *authService) ApplyPasswordReset(ctx context.Context, in *generated.TwoStrs, opts ...client.CallOption) (*generated.EmptyMessage, error) {
	panic("implement me")
}
