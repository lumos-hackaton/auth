package service

import (
	"bitbucket.org/lumos-hackaton/protobuf/generated"
	"bitbucket.org/lumos-hackaton/shared/dbase"
	"bitbucket.org/lumos-hackaton/shared/dbase/entity/user"
	"context"
	"github.com/micro/go-micro/client"
)

func (s *authService) IsEmailFree(ctx context.Context, in *generated.Email, opts ...client.CallOption) (*generated.Bool, error) {
	r, err := s.IsEmailFreeCustom(in.Email)
	if err != nil {
		return nil, err
	}

	return &generated.Bool{Is: r}, err
}

func (s *authService) IsEmailFreeCustom(email string) (bool, error) {
	var user user.User

	c := dbase.Users.User(s.db)

	err := c.Find(dbase.M{"email": email}).One(&user)
	if err != nil && err.Error() != "not found" {
		return false, err
	} else if err != nil && err.Error() == "not found" {
		return true, nil
	}

	return false, nil
}

func (s *authService) IsUsernameFree(ctx context.Context, in *generated.String, opts ...client.CallOption) (*generated.Bool, error) {
	r, err := s.IsEmailFreeCustom(in.Str)
	if err != nil {
		return nil, err
	}

	return &generated.Bool{Is: r}, err
}

func (s *authService) IsUsernameFreeCustom(username string) (bool, error) {
	var user user.User

	c := dbase.Users.User(s.db)

	err := c.Find(dbase.M{"username": username}).One(&user)
	if err != nil && err.Error() != "not found" {
		return false, err
	} else if err != nil && err.Error() == "not found" {
		return true, nil
	}

	return false, nil
}

func (s *authService) VerifyEmail(ctx context.Context, in *generated.Email, opts ...client.CallOption) (*generated.EmptyMessage, error) {
	panic("implement me")
}
