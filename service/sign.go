package service

import (
	"bitbucket.org/lumos-hackaton/protobuf/generated"
	"bitbucket.org/lumos-hackaton/protobuf/generated/auth"
	"bitbucket.org/lumos-hackaton/shared/dbase"
	"bitbucket.org/lumos-hackaton/shared/dbase/entity/user"
	"context"
	"errors"
	"github.com/micro/go-micro/client"
)

func (s *authService) Refresh(ctx context.Context, in *auth.RefreshRequest, opts ...client.CallOption) (*generated.String, error) {
	panic("implement me")
}

func (s *authService) CheckAuth(ctx context.Context, in *auth.CheckRequest, opts ...client.CallOption) (*generated.EmptyMessage, error) {
	var u user.User

	err := dbase.Users.User(s.db).Find(dbase.M{"token": in.Token}).One(&u)
	if err != nil {
		if err.Error() != "not found" {
			return nil, errors.New("Unauthenticated")
		}
		return nil, err
	}

	return nil, nil
}
