package service

import (
	"bitbucket.org/lumos-hackaton/protobuf/generated"
	"bitbucket.org/lumos-hackaton/protobuf/generated/auth"
	"bitbucket.org/lumos-hackaton/shared/dbase"
	"bitbucket.org/lumos-hackaton/shared/dbase/entity/user"
	"context"
	"errors"
	"github.com/micro/go-micro/client"
	"golang.org/x/crypto/bcrypt"
	"fmt"
)

func (s *authService) Login(ctx context.Context, in *auth.LoginRequest, opts ...client.CallOption) (*generated.String, error) {
	fmt.Println(in.Login, in.Password)

	c := dbase.Users.User(s.db)
	var u user.User
	err := c.Find(dbase.M{"email": in.Login}).One(&u)
	if err != nil {
		return nil, err
	}

	if err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(in.Password)); err != nil {
		return nil, errors.New("Incorrect password")
	}

	t, err := s.GenerateToken(in.Login, in.Password)
	if err != nil {
		return nil, err
	}

	fmt.Println(t)
	return &generated.String{Str: t}, nil
}
