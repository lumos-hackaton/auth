package service

import (
	"bitbucket.org/lumos-hackaton/shared/dbase"
	"bitbucket.org/lumos-hackaton/shared/dbase/entity/user"
	"github.com/labstack/gommon/random"
	"encoding/base64"
	"golang.org/x/crypto/sha3"
)

func (s *authService) GenerateToken(email, password string) (string, error) {
	c := dbase.Users.User(s.db)

	hasher := sha3.New512()
	hasher.Write([]byte(email + password + random.New().String(8, random.Alphanumeric)))
	token := base64.URLEncoding.EncodeToString(hasher.Sum(nil))

	var u user.User

	err := c.Find(dbase.M{"email": email}).One(&u)
	if err != nil {
		return "", err
	}

	u.Token = token

	c.UpdateId(u.Id, u)

	return u.Token, nil
}
